import Head from 'next/head'
import styles from '../styles/Home.module.css'
import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';
import Button from '@mui/material/Button';
import * as React from 'react';
import {DataGrid} from '@mui/x-data-grid'
import LinkedInIcon from '@mui/icons-material/LinkedIn';
import GitHubIcon from '@mui/icons-material/GitHub';
import {Box, FormControl, InputLabel, MenuItem, Select, TextField} from '@mui/material';
import * as gtag from '../lib/gtag'
import { ValidatorForm, TextValidator} from 'react-material-ui-form-validator';

import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';


const weapons = [
    {
        name: "Great Axe",
        "multi": false,
        "attr1": "Strength"
    },
    {
        name: "War Hammer",
        "multi": false,
        "attr1": "Strength"
    },
    {
        name: "Bow",
        "multi": false,
        "attr1": "Dexterity"
    },
    {
        name: "Ice Gauntlet",
        "multi": false,
        "attr1": "Intelligence"
    },
    {
        name: "Fire Staff",
        "multi": false,
        "attr1": "Intelligence"
    },
    {
        name: "Life Staff",
        "multi": false,
        "attr1": "Focus"
    },
    {
        name: "Spear",
        "multi": true,
        "attr1": "Dexterity",
        "attr2": "Strength"
    },
    {
        name: "Sword",
        "multi": true,
        "attr1": "Strength",
        "attr2": "Dexterity"
    },
    {
        name: "Hatchet",
        "multi": true,
        "attr1": "Strength",
        "attr2": "Dexterity"
    },
    {
        name: "Rapier",
        "multi": true,
        "attr1": "Dexterity",
        "attr2": "Intelligence"
    },
    {
        name: "Musket",
        "multi": true,
        "attr1": "Dexterity",
        "attr2": "Intelligence"
    }
];

export function WeaponPicker(props) {
    const label = props.label;
    const callback = props.callback;

    const handleChange = (event) => {
        callback(event.target.value);
    };
    return (
        <div>
            <InputLabel id="demo-simple-select-label">{label}</InputLabel>
            <Select
                required
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={props.value}
                label={label}
                onChange={handleChange}
                fullWidth
            >
                {
                    weapons.map((weapon, index) => {
                        return (
                            <MenuItem key={index} value={index}>{weapon.name}</MenuItem>
                        )
                    })
                }
            </Select>
        </div>
    )
}

export default function Home() {
    const [primaryWeapon, setPrimaryWeapon] = React.useState('');
    const [secondaryWeapon, setSecondaryWeapon] = React.useState('');
    const [level, setLevel] = React.useState('');
    const [strength, setStrength] = React.useState('');
    const [dexterity, setDexterity] = React.useState('');
    const [intelligence, setIntelligence] = React.useState();
    const [focus, setFocus] = React.useState('');
    const [constitution, setConstitution] = React.useState('');
    const [primaryBase, setPrimaryBase] = React.useState('');
    const [secondaryBase, setSecondaryBase] = React.useState('');
    const [desiredAp, setDesiredAp] = React.useState(0);
    const [sortModel, setSortModel] = React.useState([]);


    const [showResult, setShowResult] = React.useState(false);
    const [columns, setColumns] = React.useState([]);
    const [rows, setRows] = React.useState([]);

    function handleSubmit(e) {
        run()
    }

    function reset(e) {
        setPrimaryWeapon('');
        setSecondaryWeapon('');
        setLevel('');
        setStrength('');
        setDexterity('');
        setIntelligence('');
        setFocus('');
        setConstitution('');

        setPrimaryBase('');
        setSecondaryBase('');
        setDesiredAp(0);
        setShowResult(false);
        setColumns([]);
        setRows([]);
    }

    function run() {
        setShowResult(false);
        setSortModel([]);
        setColumns([]);
        setRows([]);

        const primary_data = weapons[primaryWeapon];
        const secondary_data = weapons[secondaryWeapon];

        let selectedAttributes = [];

        selectedAttributes.push(primary_data['attr1']);
        selectedAttributes.push(secondary_data['attr1']);

        if (primary_data['multi']) {
            selectedAttributes.push(primary_data['attr2']);
        }
        if (secondary_data['multi']) {
            selectedAttributes.push(secondary_data['attr2']);
        }

        selectedAttributes = selectedAttributes.filter(function (value, index, array) {
            return array.indexOf(value) === index;
        });

        const stats = {
            'Strength': parseInt(strength),
            'Dexterity': parseInt(dexterity),
            'Intelligence': parseInt(intelligence),
            'Focus': parseInt(focus),
        };

        let localColumns = [];
        let localRows = [];

        const reversedAttributes = selectedAttributes.reverse();
        for (let attribute of reversedAttributes) {
            localColumns.push({
                field: attribute, headerName: attribute, flex: 1
            })
        }
        localColumns.push({
            field: 'primaryDamage', headerName: 'Primary Weapon Damage', flex: 1
        }, {
            field: 'secondaryDamage', headerName: 'Secondary Weapon Damage', flex: 1
        }, {
            field: 'combinedDamage', headerName: 'Combined Damage', flex: 1
        })

        gtag.event({
            action: 'submit',
            category: 'damage-simulator',
            label: JSON.stringify([parseInt(desiredAp), parseInt(level), selectedAttributes, primary_data, secondary_data, stats, parseInt(primaryBase), parseInt(secondaryBase)])
        })
        let report = pop_possibilities(parseInt(desiredAp), parseInt(level), selectedAttributes, primary_data, secondary_data, stats, parseInt(primaryBase), parseInt(secondaryBase));

        setColumns(localColumns);

        let index = 0;
        for (let data of report) {
            let localObj = {};
            localObj.id = index;

            let attrCount = reversedAttributes.length;
            let attrData = data.slice(0, attrCount)

            let attrIndex = 0;
            for (let attribute of reversedAttributes) {
                localObj[attribute] = data[attrIndex];
                attrIndex++;
            }

            let damageData = data.slice(attrCount, data.length);

            localObj.primaryDamage = damageData[0];
            localObj.secondaryDamage = damageData[1];
            localObj.combinedDamage = damageData[2];

            localRows.push(localObj);
            index++;
        }
        setRows(localRows);
        setSortModel([{
            field: 'combinedDamage',
            sort: 'desc'
        }])
        setShowResult(true);
    }

    return (
        <div>
            <Box sx={{flexGrow: 1}}>
                <AppBar position="static">
                    <Toolbar>
                        <Typography variant="h6" component="div" sx={{flexGrow: 1}}>
                            New World Damage Simulator
                        </Typography>
                        <div>
                            <a href="https://gitlab.com/Magzter/newworld-tools" target="_blank"
                               rel="noopener noreferrer">
                                <IconButton
                                    size="large"
                                    aria-label="account of current user"
                                    aria-controls="menu-appbar"
                                    aria-haspopup="true"
                                    color="inherit"
                                >
                                    <GitHubIcon/>
                                </IconButton>
                            </a>
                        </div>
                    </Toolbar>
                </AppBar>
            </Box>
            <div className={styles.container}>
                <Head>
                    <title>New World Damage Simulator</title>
                    <meta name="description" content="Calculate your best attribute point setup for damage"/>
                    <meta name="viewport" content="initial-scale=1.0, width=device-width"/>
                    <link rel="icon" href="/favicon.ico"/>
                </Head>

                <main className={styles.main}>
                    <h1 className={styles.title}>
                        New World Damage Simulator
                    </h1>

                    <p className={styles.description}>
                        A tool designed to take a characters base stats including armor and weapons, level, and base
                        damage
                        of their items (slash damage, blunt damage, etc) to simulate every combination of Attribute
                        point
                        assignments and report back the one with the highest combined weapon damage.
                    </p>

                    <hr/>

                    <ValidatorForm
                        onError={errors => console.log(errors)}
                        onSubmit={(e) => handleSubmit(e)}
                        style={{width: "100%"}}
                    >
                    <Box
                        noValidate
                        autoComplete="off"
                        style={{width: "100%"}}
                    >
                        <h3 className={styles.displayText}>Character Information</h3>
                        <p className={styles.displayText}>Enter in your base stats here after clicking respec in your
                            attribute points menu</p>
                        <div className={styles.weaponInformation}>
                            <div className={styles.formElement}>
                                <TextField
                                    required
                                    id="outlined-number"
                                    label="Level"
                                    inputProps={{ min: 0, max: 60 }}
                                    type="number"
                                    value={level}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    fullWidth
                                    onChange={(e) => setLevel(e.target.value)}
                                />
                            </div>
                        </div>
                        <div className={styles.characterStats}>
                            <div className={styles.formElement}>
                                <TextField
                                    required
                                    id="outlined-number"
                                    label="Strength"
                                    type="number"
                                    inputProps={{ min: 0, max: 300 }}
                                    value={strength}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    fullWidth
                                    onChange={(e) => setStrength(e.target.value)}
                                />
                            </div>
                            <div className={styles.formElement}>
                                <TextField
                                    required
                                    id="outlined-number"
                                    label="Dexterity"
                                    type="number"
                                    value={dexterity}
                                    inputProps={{ min: 0, max: 300 }}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    fullWidth
                                    onChange={(e) => setDexterity(e.target.value)}
                                />
                            </div>
                            <div className={styles.formElement}>
                                <TextField
                                    required
                                    id="outlined-number"
                                    label="Intelligence"
                                    type="number"
                                    inputProps={{ min: 0, max: 300 }}
                                    value={intelligence}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    fullWidth
                                    onChange={(e) => setIntelligence(e.target.value)}
                                />
                            </div>
                            <div className={styles.formElement}>
                                <TextField
                                    required
                                    id="outlined-number"
                                    label="Focus"
                                    value={focus}
                                    inputProps={{ min: 0, max: 300 }}
                                    type="number"
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    fullWidth
                                    onChange={(e) => setFocus(e.target.value)}
                                />
                            </div>
                            <div className={styles.formElement}>
                                <TextField
                                    required
                                    id="outlined-number"
                                    label="Constitution"
                                    inputProps={{ min: 0, max: 300 }}
                                    value={constitution}
                                    type="number"
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    fullWidth
                                    onChange={(e) => setConstitution(e.target.value)}
                                />
                            </div>
                        </div>
                        <h3 className={styles.displayText}>Weapon Information</h3>
                        <div className={styles.weaponInformation}>
                            <div className={styles.formElement}>
                                <FormControl fullWidth>
                                    <WeaponPicker label="Primary Weapon" value={primaryWeapon}
                                                  callback={setPrimaryWeapon}/>
                                </FormControl>
                            </div>
                            <div className={styles.formElement}>
                                <TextField
                                    required
                                    id="outlined-number"
                                    label="Primary Base Damage"
                                    value={primaryBase}
                                    inputProps={{ min: 0 }}
                                    type="number"
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    fullWidth
                                    onChange={(e) => setPrimaryBase(e.target.value)}
                                />
                            </div>
                        </div>
                        <div className={styles.weaponInformation}>
                            <div className={styles.formElement}>
                                <FormControl fullWidth>
                                    <WeaponPicker label="Secondary Weapon" value={secondaryWeapon}
                                                  callback={setSecondaryWeapon}/>
                                </FormControl>
                            </div>
                            <div className={styles.formElement}>
                                <FormControl fullWidth>
                                    <TextField
                                        required
                                        id="outlined-number"
                                        label="Secondary Base Damage"
                                        value={secondaryBase}
                                        inputProps={{ min: 0 }}
                                        type="number"
                                        InputLabelProps={{
                                            shrink: true,
                                        }}
                                        fullWidth
                                        onChange={(e) => setSecondaryBase(e.target.value)}
                                    />
                                </FormControl>
                            </div>
                        </div>
                        <h3 className={styles.displayText}>Additional Options</h3>
                        <p className={styles.displayText}>Reserve attribute points for
                            constitution</p>
                        <div className={styles.weaponInformation}>
                            <div className={styles.formElement}>
                                <TextField
                                    required
                                    id="outlined-number"
                                    label="Desired AP to Constitution"
                                    type="number"
                                    inputProps={{ min: 0 }}
                                    value={desiredAp}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    fullWidth
                                    onChange={(e) => setDesiredAp(e.target.value)}
                                />
                            </div>
                        </div>
                        <div className={styles.weaponInformation}>
                            <div className={styles.formElement}>
                                <Button variant="contained" fullWidth type="submit">Submit</Button>
                            </div>
                            <div className={styles.formElement}>
                                <Button variant="contained" color="error" fullWidth onClick={(e) => reset(e)}>
                                    Reset
                                </Button>
                            </div>
                        </div>
                    </Box>
                    </ValidatorForm>
                    <hr/>
                    {showResult &&
                    <div style={{height: 1000, width: '100%'}}>
                        <DataGrid
                            rows={rows}
                            columns={columns}
                            pageSize={25}
                            rowsPerPageOptions={[25]}
                            sortModel={sortModel}
                            onSortModelChange={(model) => setSortModel(model)}
                            checkboxSelection={false}
                        />
                    </div>
                    }
                </main>

                <footer className={styles.footer}>
                    <div className={styles.footerBox}>
                        <a
                            href="https://github.com/Misterguruman"
                            target="_blank"
                            rel="noopener noreferrer"
                        >
                            Credit to Joseph P Langford for the programming logic
                            <span className={styles.logo}>
                            <GitHubIcon/>
                        </span>
                        </a>
                    </div>
                    <div className={styles.footerBox}>
                        <a
                            href="https://www.linkedin.com/in/matthew-magliolo/"
                            target="_blank"
                            rel="noopener noreferrer"
                        >
                            Converted to web app by Matthew Magliolo
                            <span className={styles.logo}>
                            <LinkedInIcon/>
                        </span>
                        </a>
                    </div>
                </footer>
            </div>
        </div>
    )
}

// NOTES:
//     https://youtu.be/CxC_Vn31ibI?t=135
//         Base dmg * Scaling * Attributes = Dmg
// Scaling = 2.5 * level
// Number at the top includes scaling for attributes, player level (2.5% per level)
// Weapons with 1 attribute scale per level sections
// ---------------------------------------------------
//     0-100:   16.25 per 10 / point x 100 -> max: 162.5 %
// 101-150: 13.0 per 10  / point x 50  -> max: 65.0   %
// 151-200: 11.7 per 10  / point x 50  -> max: 58.5   %
// 201-250: 10.4 per 10  / point x 50  -> max: 52.0   %
// 251-300: 9.1 per 10   / point x 50  -> max: 45.5   %
// 301+:    7.8 per 10   / point x
//
// Weapons with 2 attribute scale per level sections (primary) (0.9)
// -----------------------------------------------------------------
//     0-100:   14.63 / point x 100 -> max: 146.25 %
// 101-150: 11.7  / point x 50  -> max: 58.50  %
// 151-200: 10.53 / point x 50  -> max: 52.65  %
// 201-250: 9.36  / point x 50  -> max: 46.80  %
// 251-300: 8.19  / point x 50  -> max: 40.95  %
// 301+:    7.02  / point x x
//
// Weapons with 2 attribute scale per level sections (secondary) (0.65)
// --------------------------------------------------------------------
//     0-100:   10.56 / point x 100 -> max: 105.63 %
// 101-150: 8.45  / point x 50  -> max: 42.25  %
// 151-200: 7.61  / point x 50  -> max: 38.03  %
// 201-250: 6.76  / point x 50  -> max: 33.80  %
// 251-300: 5.92  / point x 50  -> max: 29.58  %
// 301+:    5.07  / point x

function calculate_available_ap(level) {
    if (level <= 13) {
        return (level * 2) - 2;
    }
    if (level <= 40) {
        return (level - 13) * 3 + 24;
    }
    if (level <= 55) {
        return (level - 40) * 4 + 105;
    }
    if (level <= 60) {
        return (level - 55) * 5 + 165;
    }
}

function calculate_level_scaling(level) {
    return (level - 1) * .025;
}

function calculate_ap_scaling(stat) {
    stat = parseInt(stat);
    if (stat <= 100) {
        let apScale = (stat - 5) * 0.01625
        return apScale;
    }
    if (stat <= 150) {
        let apScale = calculate_ap_scaling(100) + ((stat - 100) * 0.013);
        return apScale;
    }
    if (stat <= 200) {
        let apScale = calculate_ap_scaling(150) + ((stat - 150) * 0.0117)
        return apScale;
    }
    if (stat <= 250) {
        let apScale = calculate_ap_scaling(200) + ((stat - 200) * 0.0104)
        return apScale;
    }
    if (stat <= 300) {
        let apScale = calculate_ap_scaling(250) + ((stat - 250) * 0.0091)
        return apScale;
    }
    if (stat > 300) {
        let apScale = calculate_ap_scaling(300) + ((stat - 300) * 0.0078)
        return apScale;
    }
    return 0.0;
}

function calculate_damage_one_attr(primary_base_damage, lvl, primary_main_attr, base_attr_level) {
    const levelscaling = calculate_level_scaling(lvl)
    const apscaling = calculate_ap_scaling(primary_main_attr)
    return primary_base_damage * (calculate_level_scaling(lvl) + calculate_ap_scaling(primary_main_attr + base_attr_level) + 1)
}

function calculate_damage_two_attr(base_damage, lvl, attr_primary, attr_secondary, base_attr_level_primary, base_attr_level_secondary) {
    const levelscaling = calculate_level_scaling(parseInt(lvl))
    const apscaling = calculate_ap_scaling(parseInt(attr_primary) + parseInt(base_attr_level_primary))

    return base_damage * (1 + levelscaling + apscaling * 0.9 + calculate_ap_scaling(attr_secondary + base_attr_level_secondary) * 0.65)
}

function pop_possibilities(const_offset, level, selectedAttributes, primaryData, secondaryData, stats, primaryBase, secondaryBase) {
    let possibility_index = [];
    const usable_ap = calculate_available_ap(level) - const_offset;

    let selectedAttributesReversed = Object.assign([], selectedAttributes).reverse()
    for (let attribute of selectedAttributesReversed) {
        let attributeArray = [];
        for (let i = 0; i < (usable_ap + 1); i++) {
            attributeArray.push(i);
        }
        possibility_index.push(attributeArray);
    }
    let allPossibilities = cartesianProduct(possibility_index);

    allPossibilities = allPossibilities.filter(possibility => possibility.reduce((x, y) => x + y) === usable_ap);

    const base_stats_for_attr1 = stats[primaryData['attr1']];
    let base_stats_for_attr2 = null;
    const secondary_base_stats_for_attr1 = stats[secondaryData['attr1']];
    let secondary_base_stats_for_attr2 = null;

    if (primaryData['multi']) {
        base_stats_for_attr2 = stats[primaryData['attr2']];
    }
    if (secondaryData['multi']) {
        secondary_base_stats_for_attr2 = stats[secondaryData['attr2']];
    }

    return generate_simulation(allPossibilities, parseInt(level), parseInt(primaryBase), parseInt(secondaryBase), primaryData, secondaryData, base_stats_for_attr1, base_stats_for_attr2, secondary_base_stats_for_attr1, secondary_base_stats_for_attr2, selectedAttributes);
}

function cartesianProduct(a) { // a = array of array
    var i, j, l, m, a1, o = [];
    if (!a || a.length == 0) return a;

    a1 = a.splice(0, 1)[0]; // the first array of a
    a = cartesianProduct(a);
    for (i = 0, l = a1.length; i < l; i++) {
        if (a && a.length)
            for (j = 0, m = a.length; j < m; j++)
                o.push([a1[i]].concat(a[j]));
        else
            o.push([a1[i]]);
    }
    return o;
}

function generate_simulation(possible_ap_configurations, level, primary_base_damage, secondary_base_damage, primary_data, secondary_data, base_stats_for_attr1, base_stats_for_attr2 = null, secondary_base_stats_for_attr1, secondary_base_stats_for_attr2 = null, selectedAttributes) {
    let ret = [];

    for (let configuration of possible_ap_configurations) {
        let local = [];
        local.push(...configuration);

        if (primary_data['multi']) {
            let primaryAttributeIndexAttr1 = selectedAttributes.indexOf(primary_data['attr1']);
            let primaryAttributeIndexAttr2 = selectedAttributes.indexOf(primary_data['attr2']);

            const primary_main_attr = configuration[primaryAttributeIndexAttr1];
            const primary_sub_attr = configuration[primaryAttributeIndexAttr2];

            let result = calculate_damage_two_attr(primary_base_damage, level, primary_main_attr, primary_sub_attr, base_stats_for_attr1, base_stats_for_attr2);
            local.push(result)
        } else {
            let primaryAttributeIndexAttr1 = selectedAttributes.indexOf(primary_data['attr1']);
            const primary_main_attr = configuration[primaryAttributeIndexAttr1];
            local.push(calculate_damage_one_attr(primary_base_damage, level, primary_main_attr, base_stats_for_attr1))
        }

        if (secondary_data['multi']) {
            let secondaryAttributeIndexAttr1 = selectedAttributes.indexOf(secondary_data['attr1']);
            let secondaryAttributeIndexAttr2 = selectedAttributes.indexOf(secondary_data['attr2']);

            const secondary_main_attr = configuration[secondaryAttributeIndexAttr1];
            const secondary_sub_attr = configuration[secondaryAttributeIndexAttr2];
            local.push(calculate_damage_two_attr(secondary_base_damage, level, secondary_main_attr, secondary_sub_attr, secondary_base_stats_for_attr1, secondary_base_stats_for_attr2))
        } else {
            let secondaryAttributeIndexAttr1 = selectedAttributes.indexOf(secondary_data['attr1']);
            const primary_main_attr = configuration[secondaryAttributeIndexAttr1];
            local.push(calculate_damage_one_attr(secondary_base_damage, level, primary_main_attr, secondary_base_stats_for_attr1))
        }

        const last_2_elements = local.slice(local.length - 2);
        local.push(last_2_elements[0] + last_2_elements[1]);

        ret.push(local);
    }

    return ret;
}