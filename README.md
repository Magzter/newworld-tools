## New World Damage Simulator

A tool designed to take a characters base stats including armor and weapons, level, and base damage of their items (slash damage, blunt damage, etc) to simulate every combination of Attribute point assignments and report back the one with the highest combined weapon damage.

https://github.com/Misterguruman/new-world-damage-simulator

https://old.reddit.com/r/newworldgame/comments/q6r93r/i_created_a_tool_to_solve_the_am_i_using_my/

### Project Information

Requirements:
  * Node.js 12.0 or later
  * MacOS, Windows (including WSL), and Linux are supported

Built using [Next.js](https://nextjs.org/) ([React](https://reactjs.org/)) and deployed with [Vercel](https://vercel.com/). Uses [Material UI](https://mui.com/).

### Run locally

Clone the repo down

`npm install`

`npm run dev`

The website should now be accessible via localhost:3000
